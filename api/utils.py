from random import choice
from string import ascii_letters, digits


SLUG_LEN = 7
CHARS_TO_CHO0SE = ascii_letters.lower() + ascii_letters.upper() + digits


def generate_shortened_slug(url_instance):
    slug = "".join([choice(CHARS_TO_CHO0SE) for _ in range(SLUG_LEN)])

    if url_instance.__class__.objects.filter(shortened_slug=slug).exists():
        return generate_shortened_slug(url_instance)

    return slug
