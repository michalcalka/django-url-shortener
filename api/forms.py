from django import forms

from api.models import Urls


class UrlForm(forms.ModelForm):
    original_url = forms.URLField(widget=forms.URLInput(
        attrs={"class": "form-control form-control-lg", "placeholder": "Paste your URL here!"}))

    class Meta:
        model = Urls
        fields = ('original_url',)
