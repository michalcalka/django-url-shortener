from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect

from api.models import Urls
from api.forms import UrlForm


def home_view(request):
    template = 'api/home.html'

    context = dict()

    context['form'] = UrlForm()

    if request.method == 'GET':
        return render(request, template, context)

    elif request.method == 'POST':

        form = UrlForm(request.POST)

        if form.is_valid():
            url = form.save()

            shorten_url = request.build_absolute_uri('/') + url.shortened_slug

            original_url = url.original_url

            context['shorten_url'] = shorten_url
            context['original_url'] = original_url

            return render(request, template, context)

        context['errors'] = form.errors

        return render(request, template, context)


def redirect_url_view(request, shortened_slug):
    try:
        shortened = Urls.objects.get(shortened_slug=shortened_slug)

        shortened.save()

        return HttpResponseRedirect(shortened.original_url)

    except Urls.DoesNotExist:
        raise Http404('Your link is broken')
