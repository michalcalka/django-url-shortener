from django.db import models
from api.utils import generate_shortened_slug


class Urls(models.Model):
    original_url = models.URLField(blank=True, max_length=2048)
    shortened_slug = models.CharField(max_length=15, blank=True, unique=True)

    def __str__(self):
        return f'Your {self.original_url} became: {self.shortened_slug}'

    def save(self, *args, **kwargs):

        if not self.shortened_slug:
            self.shortened_slug = generate_shortened_slug(self)

        super().save(*args, **kwargs)

    class Meta:

        db_table = 'urls'
