from django.urls import path
from api.views import home_view, redirect_url_view

urlpatterns = [
    path('', home_view, name='home'),
    path('<str:shortened_slug>', redirect_url_view, name='redirect'),
]
